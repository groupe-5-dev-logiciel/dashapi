--------------------------------------------------

## API README.

 ```bash
# Installation des modules dans dependencies
> npm i
# Insatallation des modules dev-dependencies pour les développeurs
> npm i --save-dev
```

**Créer la documentation**

Les commandes suivantes permettront de générer la documentation de l'API grâce au module **apidoc**.

Documentation du module et l'utilisation des attributs : https://apidocjs.com/#params

Avant la commande 'npm' vous aurez l'équivalent avec la réel commande éxécuter
 ```bash
# Création de la Doc
# -i ./lib/Router/ = Input tous les fichiers dans le dossier Router
# -o apidoc/ = Output on sort tout dans le dossier apidoc
#> apidoc -i ./lib/Router/ -o apidoc/
> npm build-doc 

# Pour convertir la doc en markdown
#> apidoc-markdown -p apidoc -o DOCUMENTATION.md
> npm doc  
```

**Build l'API**

La compiliation de l'API permet de la convertir du TypeScript au JavaScript.

 ```bash
#> tsc
> npm build

# Lancer l'API compiler
#> node dist/src/server.js
> npm start
```


## Création d'une Table

Pour les commandes suivante nécessite d'avoir installé le module : sequelize-cli

Commande help 
 ```bash
>  npx sequelize
```

**Etape 1 : Création du fichier de migration**

 ```bash
> npx sequelize migration:create --name <nom_du_model_export>
```
Exemple : pour le model users 
 ```bash
>  npx sequelize migration:create --name create_users_table
```
**Etape 2 : Remplissage du fichier de migration**

Remplir le nouveau fichier de migration avec les propriétées identiques du model
(voir ./migrations/'#############-create_users_table.js')

**Etape 3 : Déploiement de la migration**

Executer les fichiers de migration avec la commande suivante
```bash
# Lance la migration de tous les fichiers présents dans le dossier migrations
> npx sequelize db:migrate

# Supprime toutes les tables créer par la migration
> npx sequelize db:migrate:undo:all
```

