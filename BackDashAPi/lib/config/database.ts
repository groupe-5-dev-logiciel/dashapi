import * as sequelize from "sequelize";

let database;
console.log(process.env.DATABASE_URL)
if (process.env.DATABASE_URL) {

    database = new sequelize.Sequelize(process.env.DATABASE_URL,{
        dialect:"postgres",
        protocol:"postgres",
    })

} else {

    database = new sequelize.Sequelize("dashapi", "postgres", "bunsarak", {
        host: "postgres",
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 1000
        }
    });
}
database.authenticate()
    .then(() => {
        console.log('INFO - Database connected');
    })
    .catch(err => {
        console.error('ERROR - Unable to connect to the database: ' + err.stack);
    });

export = database;
