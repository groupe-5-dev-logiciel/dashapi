const app = require('../../src/server')
import * as request from 'supertest';

describe('TEST GET users',() =>{
    it('GET users', async () => {
        const res = await request(app).get('/users').send();
        expect(res.statusCode).toEqual(200);
    });
});

describe('TEST GET pricing',() =>{
    it('GET pricing', async () => {
        const res = await request(app).get('/pricing').send();
        expect(res.statusCode).toEqual(200);
    });
});

describe('TEST GET subs',() =>{
    it('GET subs', async () => {
        const res = await request(app).get('/subs').send();
        expect(res.statusCode).toEqual(200);
    });
});

describe('TEST GET apiAccount',() =>{
    it('GET apiAccount', async () => {
        const res = await request(app).get('/apiAccount').send();
        expect(res.statusCode).toEqual(200);
    });
});

