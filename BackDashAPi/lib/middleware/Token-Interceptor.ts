import * as express from 'express';


function TokenInterceptor(request: express.Request, response: express.Response, next: express.NextFunction) {
    console.log(`${request.method}:${request.path} Auhtorize[${request.headers.authorization}] Body : ${JSON.stringify(request.body)} Params : ${JSON.stringify(request.query)}`);
    next();
}

export = TokenInterceptor;