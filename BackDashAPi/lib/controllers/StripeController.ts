import User = require('../models/user.model');
import Subscription = require('../models/subscription.model');
import Pricing = require('../models/pricing.model');

const stripe = require('stripe')('sk_test_51I7IhaBjcMK9UGHgzGlN7stidDGbVognBDLJVzOQlvcLAamNdCf2fo6IFJo2QfIlhQsvh1RNVJdLreqG48ARffs5006bhBpCgQ');

class StripeController {

    /**
     * Mettre à jour l'abonnement d'un utilisateur
     * @param id_session 
     * @param id_user 
     */
    async updateSubscriptionUser(id_session, id_user) {
        const session = await stripe.checkout.sessions.retrieve(id_session);
        const subs = await stripe.subscriptions.retrieve(session.subscription);

        if (session.payment_status === 'paid') {
            let price: Pricing = await Pricing.findOne<Pricing>({
                where: {
                    product_key: subs.plan.product
                }
            });
            let user: User = await User.findOne<User>({
                where: {
                    id: id_user
                }
            });

            if (price !== null && user !== null) {
                Subscription.update({
                    PricingId: price.getDataValue('id'),
                    start_date: new Date(),
                    last_payment: new Date()
                }, {
                    where: {
                        UserId: id_user
                    }
                });
            }
        }
    }

    async getPrices() {
        console.log('getPrices');
        const prices = await stripe.prices.list({
            limit: 3,
        });
        // console.log('mes Prix : ' + JSON.stringify(prices.data));
        return prices.data;
    }

    async getProduct() {
        console.log('getProduct');

        const products = await stripe.products.list({
            limit: 3,
        });

        // console.log('mes produits : ', products);
        return products.data;
    }
}

export = StripeController;
