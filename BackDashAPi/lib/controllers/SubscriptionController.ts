import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import User = require('../models/user.model');
import Subscription = require('../models/subscription.model');
import Pricing = require('../models/pricing.model');

const saltRounds: number = 10;
const secureKey: string = 'HashMyToken';

interface UserLoginInterface {
    email: string;
    password: string;
}

class SubController {

    async getSubscriptionByIdUser(idUser: number): Promise<any> {
        try {
            let subs: Subscription = await Subscription.findOne<Subscription>({
                where: {
                    UserId: idUser
                }
            });
            let price: Pricing = await Pricing.findOne<Pricing>({
                where: {
                    id: subs.getDataValue('PricingId')
                }
            });
            const result = {
                title: price.Title,
                price: price.Price,
                numberAPI: price.getDataValue('numberAPI'),
                start_date: subs.getDataValue('start_date')
            }
            return result;
        } catch (err) {
            console.log(err);
            return undefined;
        }
    }

    /**
     * login
     * 
     * @param email 
     * @param password 
     * @returns {String, Boolean, Token}
     */
    async login(email: string, password: string): Promise<any> {
        const user: User = await User.findOne({ where: { email: email } });
        let result: Boolean = false;
        let token: string = null;

        if (user !== null) {
            result = await this.comparePassword(password, user.password);
            token = await this.createToken(user);
        }

        if (result) {
            return { message: 'Connexion réussi', success: result, result: token, id: user.getDataValue('id'), subscription: await this.getSubscriptionByIdUser(user.getDataValue('id')) };
            // res.status(200).json({ message: 'Connexion réussi', success: result, result: token });
        } else {
            return { message: 'Identifiant de connexion invalide', success: result };
            // res.status(200).json({ message: 'Identifiant de connexion invalide', success: result });
        }
    }

    /**
     * createPassword
     * 
     * @param password
     * @returns string
     */
    async createPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, saltRounds);
    }

    /**
     * comparePassword
     * 
     * @param password 
     * @param passwordDB 
     * @returns boolean
     */
    async comparePassword(password: string, passwordDB: string): Promise<boolean> {
        return await bcrypt.compare(password, passwordDB);
    }

    /**
     * createToken
     * 
     * @param user
     * @returns string
     */
    async createToken(user: User): Promise<string> {
        const token: string = jwt.sign(
            {
                user
            }, secureKey,
            { expiresIn: '1h' });

        return token;
    }
}

export = SubController;