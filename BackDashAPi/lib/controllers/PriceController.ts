import Pricing = require('../models/pricing.model');
import * as sequelize from 'sequelize';
import User = require('../models/user.model');
import Subscription = require('../models/subscription.model');
import database = require('../config/database');

class PriceController {

    async getAllSubs(pWhere) {
        try {
            const prices = await Pricing.findAll<Pricing>({
                where: pWhere,
                // Add order conditions here....
                order: [
                    ['Price', 'ASC'],
                ]
            });
            return JSON.stringify(prices);
        } catch (err) {
            return err.message;
        }
    }


    /**
     * Vérifie si L'utilisateur Existe et qu'il a bien une ligne dans abonnement
     *  
     * @param idUser
     * @returns boolean : true if exist
     */
    async getUserSub(idUser: any): Promise<boolean> {
        if (idUser !== null && idUser !== undefined) {
            const subs = await Subscription.findOne({
                where: {
                    UserId: idUser
                }
            });
            if(subs !== null) {
                return true;
            }
        }
        return false;
    }
}

export = PriceController;
