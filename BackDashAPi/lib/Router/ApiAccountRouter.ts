import { Router, Request, Response } from 'express';
import * as sequelize from 'sequelize';
import ApiAccount = require("../models/apiAccount.model");

const ApiAccountRouter = Router();

/**
 * @api {get} api-account/ Get ApiAccount
 * @apiName GetApiAccounts
 * @apiGroup ApiAccount
 * @apiDescription Get all ApiAccounts
 *
 * @apiSuccess  {Number}    total      Size Array
 * @apiSuccess  {ApiAccount[]}    result     Return Array ApiAccounts
 * 
 * @apiError {String} httpcode      40.: Invalide information or something goes wrong
 */
ApiAccountRouter.get('/', (req: Request, res: Response) => {
    ApiAccount.findAll<ApiAccount>({})
        .then((nodes: Array<ApiAccount>) => res.json({ total: nodes.length, result: nodes }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {post} api-account/ Post ApiAccount
 * @apiName CreateApiAccount
 * @apiGroup ApiAccount
 * @apiDescription Add a new ApiAccount.
 *
 * @apiParam {String}   API_Token      Token ou clé de l'API public 
 * @apiParam {String}   API_Type       Type de l'API public
 * @apiParam {Number}   UserId         Id User 
 *
 * @apiSuccess  {ApiAccount}   result     Return ApiAccount
 * 
 * @apiError    {String} httpcode      40.: Invalide information or something goes wrong
 */
ApiAccountRouter.post('/', (req: Request, res: Response) => {
    // const params: apiAccountInterface = req.body;
    ApiAccount.create<ApiAccount>(req.body)
        .then((node: ApiAccount) => res.status(201).json(node))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * Get One ApiAccount
 * 
 * @api {GET} api-account/:id Get ApiAccount
 * @apiGroup ApiAccount
 * @apiName GetApiAccountById
 * 
 * @apiDescription Get one ApiAccount with the id
 * 
 * @apiSuccess {ApiAccount}    result        Data ApiAccount's
 * 
 * @apiError ApiAccountNotFound the <code>id</code> was not found.
 */
ApiAccountRouter.get('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);

    ApiAccount.findByPk<ApiAccount>(nodeId)
        .then((node: ApiAccount | null) => {
            if (node) {
                res.json(node);
            } else {
                res.status(404).json({ errors: ["Node not found"] });
            }
        })
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {put} api-account/{id} Update ApiAccount
 * @apiGroup ApiAccount
 * @apiName UpdateApiAccountById
 * @apiDescription Update ApiAccount by id
 *
 * @apiParam {Number}   id     ApiAccount ID
 *
 * @apiSuccess {Boolean}   success    True or False
 * 
 * @apiError {String} httpcode    40.: Invalide information or something goes wrong
 */
ApiAccountRouter.put('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    // const params: apiAccountInterface = req.body;

    const update: sequelize.UpdateOptions = {
        where: { id: nodeId },
        limit: 1
    };

    ApiAccount.update(req.body, update)
        .then(() => res.status(202).json({ success: true }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {delete} api-account/{id} Delete ApiAccount
 * @apiGroup ApiAccount
 * @apiName DeleteApiAccountById
 * @apiDescription Delete api-account by id
 *
 * @apiParam    {Integer}      id       ApiAccount id
 *
 * @apiSuccess  {Boolean}   success    True or False
 * 
 * @apiError    {String}    httpcode    40.: Invalide information or something goes wrong
 */
ApiAccountRouter.delete('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    const options: sequelize.DestroyOptions = {
        where: { id: nodeId },
        limit: 1
    };

    ApiAccount.destroy(options)
        .then(() => res.status(204).json({ data: "success" }))
        .catch((err: Error) => res.status(500).json(err));
});

export = ApiAccountRouter;