import { Router, Request, Response } from 'express';
import database = require('../config/database');
import User = require('../models/user.model');
import Pricing = require('../models/pricing.model');
import Subscription = require('../models/subscription.model');
import * as sequelize from 'sequelize';
import SubController = require('../controllers/SubscriptionController');

const UserRouter = Router();
const subController = new SubController();

interface UserLoginInterface {
    email: string;
    password: string;
}

/**
 * @api {get} users/ Get Users
 * @apiName GetUsers
 * @apiGroup User
 * @apiDescription Get all users
 *
 * @apiSuccess  {Number}    total      Size Array
 * @apiSuccess  {User[]}    result     Return Array users
 *
 * @apiError {String} httpcode      40.: Invalide information or something goes wrong
 */
UserRouter.get('/', async (req: Request, res: Response) => {
    User.findAll<User>({})
        .then((users: Array<User>) => res.json({ total: users.length, result: users }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {post} users/signup Post User
 * @apiName CreateUser
 * @apiGroup User
 * @apiDescription Add a new user.
 *
 * @apiParam {String}   first_name      User name
 * @apiParam {String}   last_name       User last name
 * @apiParam {String}   email           User email adresse
 * @apiParam {Date}     birthdate       User birthday date (yyyy-mm-dd)
 * @apiParam {String}   phone           User phone
 * @apiParam {String}   password        User password
 * @apiParam {String}   pwd1            User password Confirm
 *
 * @apiSuccess  {Boolean}   success    True or False
 * @apiSuccess  {String}    message    Message
 * @apiSuccess  {User}      result     Return user
 *
 * @apiError    {String} httpcode      40.: Invalide information or something goes wrong
 */
UserRouter.post('/signup', async (req: Request, res: Response) => {
    let transaction: sequelize.Transaction;
    if (req.body.password === req.body.pwd1) {
        try {
            //On crée une transaction
            transaction = await database.transaction();
            // const params: UserInterface = req.body;
            //Création du mot de passe
            req.body.password = await subController.createPassword(req.body.password);
            //Création de l'utilisateur
            let user: User = await User.create<User>(req.body, { transaction });
            //On récupère l'abonnement gratuit
            let price: Pricing = await Pricing.findOne({ where: { Price: 0 } });
            //Création de l'abonnement par défaut le gratuit
            let sub: Subscription = await Subscription.create<Subscription>({
                PricingId: price.getDataValue('id'),
                UserId: user.getDataValue('id')
            }, { transaction });
            // commit
            await transaction.commit();
            if (req.headers['user-agent'].startsWith('PostmanRuntime')) {
                res.status(200).json({ message: 'Bienvenue sur DashAPI !', success: true, result: user });
            } else {
                res.render('sign-up-success.html');
            }
        } catch (error) {
            console.log(error);
            await transaction.rollback();
            res.status(500).json({ message: error.message, success: false });
        }
    } else {
        res.status(500).json({ message: 'Le mot de passe n\'est pas identique.', success: false });
    }
});

/**
 * @api {post} users/login Post User
 * @apiName LoginUser
 * @apiGroup User
 * @apiDescription Login User
 *
 * @apiParam {String}   email           User email adresse
 * @apiParam {String}   password        User password
 *
 * @apiSuccess  {Boolean}   success    True or False
 * @apiSuccess  {String}    message    Message
 * @apiSuccess  {String}      result     Token
 *
 * @apiError    {String} httpcode      40.: Invalide information or something goes wrong
 */
UserRouter.post('/login', async (req: Request, res: Response) => {
    const params: UserLoginInterface = req.body;
    const resultJson = await subController.login(params.email, params.password);

    res.status(200).json(resultJson);
});

/**
 * Get One User
 *
 * @api {GET} users/:id Get User
 * @apiGroup User
 * @apiName GetUserById
 *
 * @apiDescription Get one User with the id
 *
 * @apiSuccess {User}           result        Data User's
 *
 * @apiError UserNotFound the <code>id</code> was not found.
 */
UserRouter.get('/:id', async (req: Request, res: Response) => {
    const userId: number = parseInt(req.params.id);
    try {
        let user: User = await User.findByPk<User>(userId);
        console.log(user);
        if(user === null) {
            throw new Error("Utilisateur inexistant");
        }
        let subs = await subController.getSubscriptionByIdUser(userId);
        res.status(200).json({ user, subs });
    } catch (err) {
        res.status(500).json(err.message);
    }
});

/**
 * @api {put} users/{id} Update User
 * @apiGroup User
 * @apiName UpdateUserById
 * @apiDescription Update user by id
 *
 * @apiParam {String}   first_name      User name
 * @apiParam {String}   last_name       User last name
 * @apiParam {String}   email           User email adresse
 * @apiParam {Date}     birthdate       User birthday date (yyyy-mm-dd)
 * @apiParam {String}   phone           User phone
 * @apiParam {String}   password        User password
 *
 * @apiSuccess  {Boolean}   success    True or False
 *
 * @apiError    {String} httpcode    40.: Invalide information or something goes wrong
 */
UserRouter.put('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    // const params: UserInterface = req.body;

    const update: sequelize.UpdateOptions = {
        where: { id: nodeId },
        limit: 1
    };

    User.update(req.body, update)
        .then(() => res.status(202).json({ success: true }))
        .catch((err: Error) => res.status(500).json({ success: false, err }));
});

/**
 * @api {delete} users/{id} Delete User
 * @apiGroup User
 * @apiName DeleteUserById
 * @apiDescription Delete user by id
 *
 * @apiParam    {Integer}      id       User id
 *
 * @apiSuccess  {Boolean}   success    True or False
 *
 * @apiError    {String}    httpcode    40.: Invalide information or something goes wrong
 */
UserRouter.delete('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    const options: sequelize.DestroyOptions = {
        where: { id: nodeId },
        limit: 1
    };

    User.destroy(options)
        .then(() => res.status(204).json({ success: true }))
        .catch((err: Error) => res.status(500).json({ success: false, err }));
});

export = UserRouter;
