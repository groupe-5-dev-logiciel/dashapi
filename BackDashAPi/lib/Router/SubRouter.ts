import { Router, Request, Response } from 'express';
import Subscription = require('../models/subscription.model');
import * as sequelize from 'sequelize';

const SubRouter = Router();

/**
 * @api {get} subs/ Get Subscription
 * @apiName GetSubscriptions
 * @apiGroup Subscription
 * @apiDescription Get all Subscriptions
 *
 * @apiSuccess  {Number}    total      Size Array
 * @apiSuccess  {Subscription[]}    result     Return Array Subscriptions
 * 
 * @apiError {String} httpcode      40.: Invalide information or something goes wrong
 */
SubRouter.get('/', (req: Request, res: Response) => {

    const filterQuery = req.query;
    let option;
    if (filterQuery !== {}) {
        option = filterQuery
    }

    Subscription.findAll<Subscription>({ where: option })
        .then((subs: Array<Subscription>) => res.json({ total: subs.length, result: subs }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {post} subs/ Post Subscription
 * @apiName CreateSubscription
 * @apiGroup Subscription
 * @apiDescription Add a new Subscription.
 *
 * @apiParam {Number}   PricingId      PricingId
 * @apiParam {Number}   UserId         UserId
 *
 * @apiSuccess  {Boolean}        success    True or False
 * @apiSuccess  {String}         message    Message
 * @apiSuccess  {Subscription}   result     Return Sub
 * 
 * @apiError    {String} httpcode      40.: Invalide information or something goes wrong
 */
SubRouter.post('/', (req: Request, res: Response) => {
    // const params: SubInterface = req.body;
    Subscription.create<Subscription>(req.body)
        .then((sub: Subscription) => res.status(201).json({ message: 'Votre abonnement à été accepté : ' + sub.start_date, success: true, result: sub }))
        .catch((err: Error) => res.status(500).json({ message: err.message, success: false }));
});

/**
 * Get One Subscription
 * 
 * @api {GET} subs/:id Get Subscription
 * @apiGroup Subscription
 * @apiName GetSubscriptionById
 * 
 * @apiDescription Get one Subscription with the id
 * 
 * @apiSuccess {Subscription}           result        Data Subscription's
 * 
 * @apiError SubscriptionNotFound the <code>id</code> was not found.
 */
SubRouter.get('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);

    Subscription.findByPk<Subscription>(nodeId)
        .then((sub: Subscription | null) => {
            if (sub) {
                res.json(sub);
            } else {
                res.status(404).json({ errors: ["L'id non présent"] });
            }
        })
        .catch((err: Error) => res.status(500).json(err.message));
});

/**
 * @api {put} subs/{id} Update Subscription
 * @apiGroup Subscription
 * @apiName UpdateSubscriptionById
 * @apiDescription Update Subscription by id
 *
 * @apiParam {Number}   id     Subscription ID
 *
 * @apiSuccess {Boolean}   success    True or False
 * 
 * @apiError {String} httpcode    40.: Invalide information or something goes wrong
 */
SubRouter.put('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    // const params: SubInterface = req.body;

    const update: sequelize.UpdateOptions = {
        where: { id: nodeId },
        limit: 1
    };

    Subscription.update(req.body, update)
        .then(() => res.status(202).json({ success: true }))
        .catch((err: Error) => res.status(500).json(err.message));
});

/**
 * @api {delete} subs/{id} Delete Subscription
 * @apiGroup Subscription
 * @apiName DeleteSubscriptionById
 * @apiDescription Delete sub by id
 *
 * @apiParam    {Integer}      id       Subscription id
 *
 * @apiSuccess  {Boolean}   success    True or False
 * 
 * @apiError    {String}    httpcode    40.: Invalide information or something goes wrong
 */
SubRouter.delete('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    const options: sequelize.DestroyOptions = {
        where: { id: nodeId },
        limit: 1
    };

    Subscription.destroy(options)
        .then(() => res.status(204).json({ data: "success", message: 'Abonnement supprimé' }))
        .catch((err: Error) => res.status(500).json(err.message));
});

export = SubRouter;