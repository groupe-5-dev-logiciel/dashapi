import { Router, Request, Response } from 'express';
import * as sequelize from 'sequelize';
import Pricing = require("../models/pricing.model");
import StripeController = require('../controllers/StripeController');
import PriceController = require('../controllers/PriceController');

const PricingRouter = Router();
const stripeController = new StripeController();
const priceController = new PriceController();

/**
 * @api {get} pricing/ Get Price
 * @apiName GetPrices
 * @apiGroup Price
 * @apiDescription Get all Prices
 *
 * @apiSuccess  {Number}    total      Size Array
 * @apiSuccess  {Price[]}    result    Return Array Prices
 * 
 * @apiError {String} httpcode      40.: Invalide information or something goes wrong
 */
PricingRouter.get('/', async (req: Request, res: Response) => {
    const filterQuery = req.query;
    let option;
    if (filterQuery !== {}) {
        option = filterQuery
    }
    Pricing.findAll<Pricing>({ where: option })
        .then((nodes: Array<Pricing>) => res.json({ total: nodes.length, result: nodes }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {post} pricing/ Post Price
 * @apiName CreatePrice
 * @apiGroup Price
 * @apiDescription Add a new Price.
 *
 * @apiParam {String}   Title           Titre de l'abonnement
 * @apiParam {Number}   Price           Prix de l'abonnement par mois
 * @apiParam {String}   product_key     ID du produit (Stripe)
 * @apiParam {String}   price_key       ID du prix (Stripe)
 * @apiParam {String}   numberAPI       Nombre d'APIs
 *
 * @apiSuccess  {Price}     result     Return Price
 * 
 * @apiError    {String} httpcode      40.: Invalide information or something goes wrong
 */
PricingRouter.post('/', (req: Request, res: Response) => {
    // const params: PricingInterface = req.body;
    req.body.Price = req.body.Price.valueOf();
    
    Pricing.create<Pricing>(req.body)
        .then((node: Pricing) => res.status(201).json(node))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * Get One Price
 * 
 * @api {GET} pricing/:id Get Price
 * @apiGroup Price
 * @apiName GetPriceById
 * 
 * @apiDescription Get one Price with the id
 * 
 * @apiSuccess {Price}           result        Data Price's
 * 
 * @apiError PriceNotFound the <code>id</code> was not found.
 */
PricingRouter.get('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    console.log(req.params);

    Pricing.findByPk<Pricing>(nodeId)
        .then((node: Pricing | null) => {
            if (node) {
                res.json(node);
            } else {
                res.status(404).json({ errors: ["Node not found"] });
            }
        })
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * Get Synchro Price
 * 
 * @api {GET} pricing/synchronize/price Get Synchronize
 * @apiGroup Price
 * @apiName GetPriceSynchro
 * 
 * @apiDescription Synchronise les prix avec Stripe
 * 
 * @apiSuccess {String}           message        message
 * @apiSuccess {Boolean}          success        true or false
 */
PricingRouter.get('/synchronize/price', async (req: Request, res: Response) => {
    let prices = await stripeController.getPrices();
    let products = await stripeController.getProduct();
    let subs: Array<Pricing> = JSON.parse(await priceController.getAllSubs({}));

    try {
        products.forEach(async function (item: any) {
            let found: Boolean = false;
            let sub = undefined;
            subs.forEach(element => {
                if (element.product_key == item.id) {
                    sub = element;
                    found = true;
                }
            });
            let priceData: Pricing = <Pricing>({ Title: item.name, product_key: item.id, Price: 0, numberAPI: '', price_key: undefined });
            let price = prices.find((element: any) => element.product === item.id);
            if (price !== undefined) {
                priceData.Price = price.unit_amount * 0.01;
                priceData.price_key = price.id;
            }

            if (item.metadata.numberAPI !== undefined) {
                priceData.numberAPI = item.metadata.numberAPI;
            }

            if (found) {
                await Pricing.update(priceData, { where: { id: sub.id } })
            } else {
                await Pricing.create<Pricing>(priceData);
            }
        });
        res.status(200).json({ message: 'Synchro', success: true });
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: e.message, success: false })
    }
});

/**
 * @api {put} pricing/{id} Update Price
 * @apiGroup Price
 * @apiName UpdatePriceById
 * @apiDescription Update Price by id
 *
 * @apiParam {Number}   PriceId     PriceId
 *
 * @apiSuccess {Boolean}   success    True or False
 * 
 * @apiError {String} httpcode    40.: Invalide information or something goes wrong
 */
PricingRouter.put('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    // const params: PricingInterface = req.body;

    const update: sequelize.UpdateOptions = {
        where: { id: nodeId },
        limit: 1
    };

    Pricing.update(req.body, update)
        .then(() => res.status(202).json({ data: "success" }))
        .catch((err: Error) => res.status(500).json(err));
});

/**
 * @api {delete} pricing/{id} Delete Price
 * @apiGroup Price
 * @apiName DeletePriceById
 * @apiDescription Delete Price by id
 *
 * @apiParam    {Integer}      id       Price id
 *
 * @apiSuccess  {String}    message    message result
 * @apiSuccess  {Boolean}   success    True or False
 * 
 * @apiError    {String}    httpcode    40.: Invalide information or something goes wrong
 */
PricingRouter.delete('/:id', (req: Request, res: Response) => {
    const nodeId: number = parseInt(req.params.id);
    const options: sequelize.DestroyOptions = {
        where: { id: nodeId },
        limit: 1
    };

    Pricing.destroy(options)
        .then(() => res.status(204).json({ success: true, message: 'Pricing supprimé' }))
        .catch((err: Error) => res.status(500).json(err));
});

export = PricingRouter;

