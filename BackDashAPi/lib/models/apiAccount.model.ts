import * as sequelize from 'sequelize';
import database = require('../config/database');

interface apiAccountInterface {
    idAccount: number;
    API_Token: string;
}

class ApiAccount extends sequelize.Model {
    public idAPI_Account!: number;
    public idUser!: number;
    public API_Token!: string;
    public API_Type!: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

ApiAccount.init({
    id: {
        type: sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    idUser: {
        type: new sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'Users',
            key: 'id',
            deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE(),
        }
    },
    API_Token: {
        type: new sequelize.DataTypes.TEXT,
        allowNull: false
    },
    API_Type: {
        type: new sequelize.DataTypes.STRING(90),
        allowNull: true
    }
}, {
    tableName: "API_Account",
    sequelize: database
})

export = ApiAccount;
