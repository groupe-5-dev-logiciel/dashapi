import * as sequelize from 'sequelize';
import database = require('../config/database');

interface PricingInterface {
    Title: string;
    Price: number;
}

class Pricing extends sequelize.Model {
    public id!: number;
    public Title!: string;
    public Price!: number;
    public product_key!: string;
    public price_key!: number;
    public numberAPI!: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Pricing.init({
    id: {
        type: sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    Title: {
        type: new sequelize.DataTypes.STRING(45),
        allowNull: false
    },
    Price: {
        type: new sequelize.DataTypes.INTEGER,
        allowNull: false
    },
    product_key: {
        type: new sequelize.DataTypes.TEXT,
        allowNull: true
    },
    price_key: {
        type: new sequelize.DataTypes.TEXT,
        allowNull: true
    },
    numberAPI: {
        type: new sequelize.DataTypes.STRING(45),
        allowNull: true
    }
}, {
    tableName: "Pricing",
    sequelize: database
});

export = Pricing;
