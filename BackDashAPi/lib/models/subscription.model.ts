import database = require('../config/database');
import * as sequelize from 'sequelize';
import User = require('../models/user.model');
import Pricing = require('../models/pricing.model');

interface SubInterface {
    AccountId: number;
    PricingId: number;
    start_date: Date;
}

class Subscription extends sequelize.Model {
    public id!: number;
    public UserId!: number;
    public PricingId!: number;
    public start_date!: Date;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Subscription.init({
    id: {
        type: sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    PricingId: {
        type: sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Pricing,
            key: 'id',
            deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE()
        }
    },
    UserId: {
        type: sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: User,
            key: 'id',
            deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE()
        }
    },
    start_date: {
        type: sequelize.DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    end_date: {
        type: sequelize.DataTypes.DATE,
        allowNull: true
    },
    last_payment: {
        type: sequelize.DataTypes.DATE,
        allowNull: true
    }
},
    {
        tableName: "Subscriptions",
        sequelize: database
    });

export = Subscription;
