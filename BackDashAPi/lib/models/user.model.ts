import * as sequelize from 'sequelize';
import database = require('../config/database');

interface UserInterface {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  password: string;
  birth_date: Date;
}

class User extends sequelize.Model {
  public id!: number;
  public first_name!: string;
  public last_name!: string;
  public birth_date!: Date;
  public email!: string;
  public phone!: string;
  public password!: string;
  public token!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

User.init(
  {
    id: {
      type: sequelize.DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    first_name: {
      type: sequelize.DataTypes.STRING(128),
      allowNull: false
    },
    last_name: {
      type: sequelize.DataTypes.STRING(128),
      allowNull: false
    },
    birthdate: {
      type: sequelize.DataTypes.DATEONLY,
      allowNull: true
    },
    email: {
      type: sequelize.DataTypes.STRING(128),
      allowNull: false,
      unique: true
    },
    phone: {
      type: sequelize.DataTypes.STRING(128),
      allowNull: true
    },
    password: {
      type: sequelize.DataTypes.STRING(128),
      allowNull: false
    },
    token: {
      type: sequelize.DataTypes.TEXT
    }
  },
  {
    tableName: "Users",
    sequelize: database // this bit is important
  }
);

export = User;
