
const stripe = Stripe("pk_test_51I7IhaBjcMK9UGHgwKLthCBD1nJfG8qhVeYai93MG34QwnHXAyo5GQ1O82zN4m0WyaMRDcUOlMr036TjKB9gAg0v00gGSY5uSC");
const socket = io();

socket.on("loadSubs", function (data) {
  prices = JSON.parse(data);
  if (prices.length === 0) {
    noSub()
  } else {
    prices.forEach(element => {
      addElement(element);
    })
  }
});

socket.on("isUser", function (data) {
  if (data.isUser) {
    goToStripe(data.idProduct);
  } else {
    alert("Vous devez être connecté ou vous inscrire pour souscrire à un abonnement !");
  }
});

socket.on("onLogin", function (data) {
  if (data.success) {
    closeForm();
    document.getElementById('idUser').value = data.id;
    document.getElementById('btn-co').style.display = "none";
  } else {
    alert(data.message);
  }
});

function noSub() {
  var PricingCard = document.createElement("div");
  PricingCard.className = "pricing-card";
  PricingCard.id = "pricing-card";

  var PricingHeader = document.createElement("h3");
  PricingHeader.className = "pricing-card-header";
  PricingHeader.innerHTML = "Pas d'abonnement";

  var divPrice = document.createElement("div");
  divPrice.className = "content";
  divPrice.innerHTML = "<p>Il n'y a pas d'abonnement disponible pour l'instant.</p>";

  PricingCard.appendChild(PricingHeader);
  PricingCard.appendChild(divPrice);

  var currentDiv = document.getElementById('listPrice');
  currentDiv.appendChild(PricingCard);
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function login() {
  let mail = document.getElementById("email").value;
  let pwd = document.getElementById("pwd").value;
  console.log(mail + ' ' + pwd);
  if (mail !== "" && pwd !== "") {
    socket.emit("login", { login: mail, password: pwd });
  }
}

function addElement(data) {
  // Exemple : 
  // <div class="pricing-card">
  //   <h3 class="pricing-card-header">Voyageur</h3>
  //   <div class="price"><sup></sup>5€<span>/mois</span></div>
  //   <h1 class="content"><strong>15</strong> APIs</h1>
  //   <a id="voyageur-button" href="#" class="order-btn">S'abonner</a>
  // </div>

  // crée un nouvel élément div
  var PricingCard = document.createElement("div");
  PricingCard.className = "pricing-card";
  PricingCard.id = "pricing-card";
  // crée un nouvel élément Titre
  var PricingHeader = document.createElement("h3");
  PricingHeader.className = "pricing-card-header";
  PricingHeader.innerHTML = data.Title;

  //Div qui contient le prix par mois
  var divPrice = document.createElement("div");
  divPrice.className = "price";
  divPrice.innerHTML = "<sup></sup>" + data.Price + "€<span>/mois</span>";
  //<div class="price"><sup></sup>5€<span>/mois</span></div>
  var ApiHeader = document.createElement("p");
  ApiHeader.className = "content";
  ApiHeader.innerHTML = "<strong>" + data.numberAPI + "</strong> APIs"
  //<h1 class="content"><strong>15</strong> APIs</h1>

  var Btn = document.createElement("a");
  Btn.className = "order-btn";
  Btn.text = "S'abonner";
  Btn.setAttribute('onclick', "createSub('" + data.price_key + "')");

  //<a id="voyageur-button" href="#" class="order-btn">S'abonner</a>
  PricingCard.appendChild(PricingHeader);
  PricingCard.appendChild(ApiHeader);
  PricingCard.appendChild(divPrice);
  PricingCard.appendChild(Btn);

  // ajoute le nouvel élément créé et son contenu dans le DOM
  var currentDiv = document.getElementById('listPrice');
  currentDiv.appendChild(PricingCard);
}

//Fonction appelé par le bouton
function createSub(id_product) {
  let idUser = document.getElementById('idUser').value;
  if (idUser !== "" && idUser !== undefined) {
    //On vérifie si l'utilisateur existe
    socket.emit("idUser", { idUser: idUser, idProduct: id_product });
  } else {
    alert("Vous devez être connecté ou vous inscrire pour souscrire à un abonnement !");
  }
}


/**
 * goToStripe
 * 
 * @param {string} id_product 
 */
function goToStripe(id_product) {
  let idUser = document.getElementById('idUser').value;
  createCheckoutSession(id_product, idUser).then(function (data) {
    // Call Stripe.js method to redirect to the new Checkout page
    stripe.redirectToCheckout({
        sessionId: data.id
      })
      .then(handleResult);
  }).catch(errr => {
    console.log('ERROR ' + errr.message);
  });
}

// Create a Checkout Session with the selected plan ID
async function createCheckoutSession(priceId, idUser) {
  return await fetch("/stripe/create-checkout-session", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      priceId: priceId,
      idUser: idUser
    })
  }).then(handleFetchResult);
};

function handleFetchResult(result) {
  if (!result.ok) {
    return result.json().then(function (json) {
      if (json.error && json.error.message) {
        throw new Error(result.url + ' ' + result.status + ' ' + json.error.message);
      }
    }).catch(function (err) {
      showErrorMessage(err);
      throw err;
    });
  }
  return result.json();
};

// Handle any errors returned from Checkout
function handleResult(result) {
  if (result.error) {
    showErrorMessage(result.error.message);
  }
};

var showErrorMessage = function (message) {
  var errorEl = document.getElementById("error-message")
  errorEl.textContent = message;
  errorEl.style.display = "block";
};