import * as express from 'express';
import { Application, Request, Response } from 'express';
import * as bodyParser from 'body-parser';
import TokenInterceptor = require('../lib/middleware/Token-Interceptor');
import * as sequelize from 'sequelize';
import PriceController = require('../lib/controllers/PriceController');
import SubscriptionController = require('../lib/controllers/SubscriptionController');
import StripeController = require('../lib/controllers/StripeController');

const priceController = new PriceController();
const SubController = new SubscriptionController();
const stripeController = new StripeController();

const PORT = process.env.PORT || 3000;
const app: Application = express();

app.set("port", PORT);

let http = require('http').Server(app);
let io = require('socket.io')(http);

app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }));
app.use(TokenInterceptor);

app.get('/', (req: Request, res: Response) => {
    res.send('Welcome to [DashApi] - API');
});
app.get('/documentation', (req: Request, res: Response) => {
    res.redirect('https://gitlab.com/groupe-5-dev-logiciel/dashapi/-/wikis/Documentation-API');
});

app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(express.static('public'));

app.get('/about', (req: Request, res: Response) => {
    res.render('about.html');
});
app.get('/success', (req: Request, res: Response) => {
    stripeController.updateSubscriptionUser(req.query.session_id, req.query.a);
    res.render('success.html');
});
app.get('/failure', (req: Request, res: Response) => {
    res.render('failure.html');
});
app.get('/sign-up', (req: Request, res: Response) => {
    res.render('sign-up.html');
});

import * as UserRouter from '../lib/Router/UserRouter';
import * as PricingRouter from "../lib/Router/PricingRouter";
import * as SubRouter from '../lib/Router/SubRouter';
import * as ApiAccountRouter from "../lib/Router/ApiAccountRouter";
import * as Stripe from '../lib/Router/stripe';

/**
 * Routes
 */
app.use('/users', UserRouter);
app.use('/pricing', PricingRouter);
app.use('/apiAccount', ApiAccountRouter);
app.use('/subs', SubRouter);
app.use('/stripe', Stripe);

// whenever a user connects on port 3000 via
// a websocket, log that a user has connected
io.on("connection", async function (socket: any) {
    //A la connection d'un utilisateur on envoie les différents abonnements
    let where = {
        Price: {
            [sequelize.Op.ne]: 0
        }
    }
    socket.emit("loadSubs", await priceController.getAllSubs(where));

    socket.on("idUser", async function (data: any) {
        socket.emit("isUser", { isUser: await priceController.getUserSub(data.idUser), idProduct: data.idProduct});
    });

    socket.on("login", async function (data: any) {
        socket.emit("onLogin", await SubController.login(data.login, data.password));
    });
});


http.listen(PORT, function () {
    console.log("listening on PORT : " + PORT);
});


export = app;