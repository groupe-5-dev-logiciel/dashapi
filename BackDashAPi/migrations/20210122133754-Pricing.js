'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("Pricing", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      Title: {
        type: Sequelize.STRING(45),
        allowNull: false
      },
      Price: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      product_key: new Sequelize.TEXT,
      price_key: Sequelize.TEXT,
      numberAPI: {
        type: Sequelize.STRING(45)
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Pricing");
  }
};
