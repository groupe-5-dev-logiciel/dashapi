﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Converts the <see cref="ApplicationPage"/> to an actual view/page
    /// </summary>
    public class ApplicationPageValueConverter : BaseValueConverter<ApplicationPageValueConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Find the appropriate page
            switch ((ApplicationPage)value)
            {
                case ApplicationPage.Dashboard:
                    return new Dashboard();

                case ApplicationPage.Bot:
                    return new Bot();

                case ApplicationPage.Profile:
                    return new Profile();

                case ApplicationPage.Contact:
                    return new Contact();

                case ApplicationPage.Settings:
                    return new Settings();

                default:
                    Debugger.Break();
                    return null;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
