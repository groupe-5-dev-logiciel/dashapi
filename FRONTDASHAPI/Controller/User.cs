﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FRONTDASHAPI
{
    public class User
    {
        private HttpClient client = new HttpClient();

        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string abonnement { get; set; }

        public async Task<User> GetUserAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://dashapi13.herokuapp.com/users/" + App.id);
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();
            var parsedData = JObject.Parse(result);
            var user = new User();
            user.id = Convert.ToInt32(parsedData["user"]["id"]);
            user.first_name = parsedData["user"]["first_name"].ToString();
            user.last_name = parsedData["user"]["last_name"].ToString();
            user.email = parsedData["user"]["email"].ToString();
            user.abonnement = parsedData["subs"]["title"].ToString();
            return user;
        }
    }
}
