﻿using FRONTDASHAPI.WpfDashboardControl.Widgets;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Controller de la fenêtre du Tchat Bot
    /// </summary>
    public class BotController
    {
        //Attributs de la classe
        private List<string> listCommand = new List<string>();
        private List<string> descCommand = new List<string>();
        private string noCommand;

        /// <summary>
        /// Constructeur de la classe.
        /// Il permet d'initialiser la liste des commandes disponibles
        /// </summary>
        public BotController()
        {
            listCommand.Add("help");
            listCommand.Add("meteo");
            listCommand.Add("spotify");

            descCommand.Add("- Commande d'aide");
            descCommand.Add("<Ville> - Commande de météo d'une ville");
            descCommand.Add("<play / pause / previous / next> - Commande du player Spotify");

            noCommand = "Erreur";
        }

        /// <summary>
        /// Fonction qui permet de déterminer la commande qui a été utilisé par l'utilisateur
        /// </summary>
        /// <param name="userCommand">String contenant la commande a exécuté</param>
        /// <returns>Retourne l'index de la commande</returns>
        private string getCommand(string userCommand)
        {
            if(userCommand.StartsWith("/"))
            {
                userCommand = userCommand.Substring(1);

                foreach (string command in this.listCommand)
                {
                    if (userCommand.Contains(command))
                        return command;
                }
            }

            return this.noCommand;
        }

        /// <summary>
        /// Fonction permettant d'établir la réponse du Bot
        /// </summary>
        /// <param name="userCommand">String contenant la commande a éxécuté</param>
        /// <returns>Retourne la réponse du Bot</returns>
        public string sendResponse(string userCommand)
        {
            string command = getCommand(userCommand);

            switch (command)
            {
                case "help":
                    string allCommand = "";
                    foreach (string oneCommand in this.listCommand)
                    {
                        allCommand += "- /" + oneCommand + " " + this.descCommand[this.listCommand.IndexOf(oneCommand)] + "\n";
                    }
                    return "Voici la liste des commande : \n" + allCommand;
                case "meteo":
                    string response = "";
                    
                     response = GetWeatherDataAsync(userCommand.Substring(7));

                    return response;
                case "spotify":
                    return HandleSpotifyCommand(userCommand.Substring(9));
                default:
                    break;
            }

            return "Cette commande n'est pas valide, Veuillez retenter dans quelques secondes.";
        }

        /// <summary>
        /// Fonction qui retourne la réponse selon la ville sélectionné
        /// </summary>
        /// <param name="city">String contenant la ville</param>
        /// <returns>La réponse</returns>
        public string GetWeatherDataAsync(string city)
        {
            string response = "";

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (var res = client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={city}&units=metric&lang=fr&appid=6c75395d09f4a804233cf32eda690ae2").Result)
                    {
                        using (HttpContent content = res.Content)
                        {
                            var data = content.ReadAsStringAsync().Result;

                            if (data != null)
                            {
                                var parsedData = JObject.Parse(data);
                                var weatherDescription = parsedData["weather"][0]["description"].ToString();
                                string WeatherDescription = weatherDescription.First().ToString().ToUpper() + weatherDescription.Substring(1);

                                response += "Météo à " + city + ", " + WeatherDescription +
                                    "\n Température Actuel : " + $"{parsedData["main"]["temp"]} °C" +
                                    "\n Température Max : " + $"{parsedData["main"]["temp_min"]} °C" +
                                    "\n Température Min : " + $"{parsedData["main"]["temp_max"]} °C" +
                                    "\n Humidité : " + $"{parsedData["main"]["humidity"]} %" +
                                    "\n Pression atmosphérique : " + $"{parsedData["main"]["pressure"]} hPa" +
                                    "\n Vitesse du vent : " + $"{parsedData["wind"]["speed"]} km/h" +
                                    "\n Direction du vent : " + $"{parsedData["wind"]["deg"]} degrés";
                            }
                        }
                    }
                }

                return response;
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public string HandleSpotifyCommand(string command)
        {
            var spotifyClient = App.APIsSettings.spotifyAPISettings.spotifyClient;

            switch (command)
            {
                case "play":
                    spotifyClient.Player.ResumePlayback();
                    return "Musique mis en lecture";
                case "pause":
                    spotifyClient.Player.PausePlayback();
                    return "Musique mis en pause";
                case "previous":
                    spotifyClient.Player.SkipPrevious();
                    return "Musique précedente";
                case "next":
                    spotifyClient.Player.SkipNext();
                    return "Musique suivante";
                default:
                    return "La commande n'existe pas";
            }
        }
    }
}
