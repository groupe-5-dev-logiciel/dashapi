﻿namespace FRONTDASHAPI
{
    /// <summary>
    /// A page of the application
    /// </summary>
    public enum ApplicationPage
    {
        /// <summary>
        /// The initial dashboard page
        /// </summary>
        Dashboard = 0,

        /// <summary>
        /// The main bot page
        /// </summary>
        Bot = 1,

        /// <summary>
        /// The main contact page
        /// </summary>
        Contact = 3,

        /// <summary>
        /// The main profile page
        /// </summary>
        Profile = 2,

        /// <summary>
        /// The main settings page
        /// </summary>
        Settings = 4,
    }
}
