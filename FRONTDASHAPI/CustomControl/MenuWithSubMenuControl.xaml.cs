﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI.CustomControl
{
    /// <summary>
    /// Logique d'interaction pour MenuWithSubMenuControl.xaml
    /// </summary>
    public partial class MenuWithSubMenuControl : UserControl
    {
        public MenuWithSubMenuControl()
        {
            InitializeComponent();
            var sideMenuViewModel = new SideMenuViewModel();
            DataContext = sideMenuViewModel;
            sideMenuViewModel.MenuList[0].Command.Execute(null);
        }

        public Thickness SubMenuPadding
        {
            get { return (Thickness)GetValue(SubMenuPaddingProperty); }
            set { SetValue(SubMenuPaddingProperty, value); }
        }

        public static readonly DependencyProperty SubMenuPaddingProperty = DependencyProperty.Register("SubMenuPadding", typeof(Thickness), typeof(MenuWithSubMenuControl));

        public bool HasIcon
        {
            get { return (bool)GetValue(HasIconProperty); }
            set { SetValue(HasIconProperty, value); }
        }

        public static readonly DependencyProperty HasIconProperty = DependencyProperty.Register("HasIcon", typeof(bool), typeof(MenuWithSubMenuControl));
    }
}
