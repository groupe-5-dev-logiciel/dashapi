﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Stripe;

namespace FRONTDASHAPI.CustomControl
{
    /// <summary>
    /// Logique d'interaction pour SubscriptionCardControl.xaml
    /// </summary>
    public partial class SubscriptionCardControl : UserControl
    {
        public SubscriptionCardControl()
        {
            InitializeComponent();
        }
        
        public string CardName 
        {
            get { return (string)GetValue(CardNameProperty); } 
            set { SetValue(CardNameProperty, value); } 
        }

        public static readonly DependencyProperty CardNameProperty = DependencyProperty.Register("CardName", typeof(string), typeof(SubscriptionCardControl), new PropertyMetadata(""));

        public string APIContent
        {
            get { return (string)GetValue(APIContentProperty); }
            set { SetValue(APIContentProperty, value); }
        }

        public static readonly DependencyProperty APIContentProperty = DependencyProperty.Register("APIContent", typeof(string), typeof(SubscriptionCardControl), new PropertyMetadata(""));

        public string Price
        {
            get { return (string)GetValue(PriceProperty); }
            set { SetValue(PriceProperty, value); }
        }

        public static readonly DependencyProperty PriceProperty = DependencyProperty.Register("Price", typeof(string), typeof(SubscriptionCardControl), new PropertyMetadata(""));

        public string SubState
        {
            get { return (string)GetValue(SubStateProperty); }
            set { SetValue(SubStateProperty, value); }
        }

        public static readonly DependencyProperty SubStateProperty = DependencyProperty.Register("SubState", typeof(string), typeof(SubscriptionCardControl), new PropertyMetadata(""));
    }
}
