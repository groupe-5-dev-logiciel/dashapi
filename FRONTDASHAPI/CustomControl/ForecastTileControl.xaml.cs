﻿using FRONTDASHAPI.WpfDashboardControl.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI.CustomControl
{
    /// <summary>
    /// Logique d'interaction pour ForecastTileControl.xaml
    /// </summary>
    public partial class ForecastTileControl : UserControl
    {
        public ForecastTileControl()
        {
            InitializeComponent();
        }

        public string DayOfWeek
        {
            get { return (string)GetValue(DayOfWeekProperty); }
            set { SetValue(DayOfWeekProperty, value); }
        }

        public static readonly DependencyProperty DayOfWeekProperty = DependencyProperty.Register("DayOfWeek", typeof(string), typeof(ForecastTileControl), new PropertyMetadata(""));

        public string Date
        {
            get { return (string)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }

        public static readonly DependencyProperty DateProperty = DependencyProperty.Register("Date", typeof(string), typeof(ForecastTileControl), new PropertyMetadata(""));

        public string WeatherIcon
        {
            get { return (string)GetValue(WeatherIconProperty); }
            set { SetValue(WeatherIconProperty, value); }
        }

        public static readonly DependencyProperty WeatherIconProperty = DependencyProperty.Register("WeatherIcon", typeof(string), typeof(ForecastTileControl), new PropertyMetadata(""));

        public string TemperatureMin
        {
            get { return (string)GetValue(TemperatureMinProperty); }
            set { SetValue(TemperatureMinProperty, value); }
        }

        public static readonly DependencyProperty TemperatureMinProperty = DependencyProperty.Register("TemperatureMin", typeof(string), typeof(ForecastTileControl), new PropertyMetadata(""));

        public string TemperatureMax
        {
            get { return (string)GetValue(TemperatureMaxProperty); }
            set { SetValue(TemperatureMaxProperty, value); }
        }

        public static readonly DependencyProperty TemperatureMaxProperty = DependencyProperty.Register("TemperatureMax", typeof(string), typeof(ForecastTileControl), new PropertyMetadata(""));
    }
}
