﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FRONTDASHAPI
{
    public class SideMenuViewModel
    {
        ResourceDictionary dict = Application.LoadComponent(new Uri("/FRONTDASHAPI;component/Assets/IconDictionary.xaml", UriKind.RelativeOrAbsolute)) as ResourceDictionary;

        public List<MenuItemsData> MenuList 
        {
            get
            {
                return new List<MenuItemsData>
                {
                    new MenuItemsData(){PathData = (PathGeometry)dict["icon_dashboard"], MenuText="Dashboard"},
                    new MenuItemsData(){PathData = (PathGeometry)dict["icon_mails"], MenuText="Subscribe"},
                    new MenuItemsData(){PathData = (PathGeometry)dict["icon_settings"], MenuText="Settings"},
                };
            }
        }
    }

    public class MenuItemsData
    {
        public PathGeometry PathData { get; set; }
        public string MenuText { get; set; }
        public MenuItemsData()
        {
            Command = new CommandViewModel(Execute);
        }

        public ICommand Command { get; }

        private void Execute()
        {
            //our logic comes here
            string MT = MenuText.Replace(" ", string.Empty);
            if (MT == "Subscribe")
            {
                redirectToSubscription();
            } else
            {
                if (!string.IsNullOrEmpty(MT))
                {
                    navigateToPage(MT);
                }
            }
            
           
        }

        private void navigateToPage(string Menu)
        {
            //We will search for our Main Window in open windows and then will access the frame inside it to set the navigation to desired page..
            //lets see how... ;)
            foreach (Window window in Application.Current.Windows)
            {
                if (window.GetType() == typeof(MainWindow))
                {
                    (window as MainWindow).MainFrame.Navigate(new Uri(string.Format("{0}{1}{2}", "Pages/", Menu, ".xaml"), UriKind.RelativeOrAbsolute));
                }
            }
        }

        private void redirectToSubscription()
        {
            Process.Start("explorer", "https://dashapi13.herokuapp.com/about");
        }
    }
}
