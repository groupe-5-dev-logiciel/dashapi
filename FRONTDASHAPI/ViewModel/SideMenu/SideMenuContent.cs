﻿namespace FRONTDASHAPI
{
    /// <summary>
    /// The type of content in the side menu
    /// </summary>
    public enum SideMenuContent
    {
        /// <summary>
        /// A list of Connected API
        /// </summary>
        Dashboard = 1,

        /// <summary>
        /// A list of chat bot commands
        /// </summary>
        Bot = 2,

        /// <summary>
        /// A list of Contact
        /// </summary>
        Contact = 4,

        /// <summary>
        /// Navigate to your profile
        /// </summary>
        Profile = 3,

        /// <summary>
        /// A list of Settings
        /// </summary>
        Settings = 5
    }
}
