﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

using FRONTDASHAPI.WpfDashboardControl.Dashboards;
using FRONTDASHAPI.WpfDashboardControl.Infrastructure;
using FRONTDASHAPI.WpfDashboardControl.Widgets;

namespace FRONTDASHAPI
{
    class DashboardViewModel : ViewModelBase
    {
        #region Private Fields

        private static DashboardsViewModel _dashboardsContent;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the content of the dashboards.
        /// </summary>
        /// <value>The content of the dashboards.</value>
        public DashboardsViewModel DashboardsContent
        {
            get => _dashboardsContent;
            set => RaiseAndSetIfChanged(ref _dashboardsContent, value);
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public DashboardViewModel()
        {
            DashboardsContent = new DashboardsViewModel();
            DashboardsContent.Start();
        }
        #endregion Public Constructors
    }
}
