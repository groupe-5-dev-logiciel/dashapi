﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;

namespace FRONTDASHAPI
{
    /// <summary>
    /// The View Model for the custom flat window
    /// </summary>
    public class WindowViewModel : BaseViewModel
    {
        #region Private Member

        /// <summary>
        /// The window this view model controls
        /// </summary>
        private Window mWindow;

        /// <summary>
        /// The window resizer helper that keeps the window size correct in various states
        /// </summary>
        private WindowResizer mWindowResizer;

        /// <summary>
        /// The margin around the window to allow for a drop shadow
        /// </summary>
        private int mOuterMarginSize = 10;

        /// <summary>
        /// The radius of the edges of the window
        /// </summary>
        private int mWindowRadius = 10;

        /// <summary>
        /// The last known dock position
        /// </summary>
        private WindowDockPosition mDockPosition = WindowDockPosition.Undocked;

        #endregion

        #region Public Properties

        /// <summary>
        /// The smallest width the window can go to
        /// </summary>
        public double WindowMinimumWidth { get; set; } = 1280;

        /// <summary>
        /// The smallest height the window can go to
        /// </summary>
        public double WindowMinimumHeight { get; set; } = 720;

        /// <summary>
        /// True if the window should be borderless because it is docked or maximized
        /// </summary>
        public bool Borderless { get { return (mWindow.WindowState == WindowState.Maximized || mDockPosition != WindowDockPosition.Undocked); } }

        /// <summary>
        /// The size of the resize border around the window
        /// </summary>
        public int ResizeBorder { get { return Borderless ? 0 : 6; } }

        /// <summary>
        /// The size of the resize border around the window, taking into account the outer margin
        /// </summary>
        public Thickness ResizeBorderThickness { get { return new Thickness(ResizeBorder + OuterMarginSize); } }

        /// <summary>
        /// The padding of the inner content of the main window
        /// </summary>
        public Thickness InnerContentPadding { get; set; } = new Thickness(0);

        /// <summary>
        /// The margin around the window to allow for a drop shadow
        /// </summary>
        public int OuterMarginSize
        {
            get
            {
                // If it is maximized or docked, no border
                return Borderless ? 0 : mOuterMarginSize;
            }
            set
            {
                mOuterMarginSize = value;
            }
        }

        /// <summary>
        /// The margin around the window to allow for a drop shadow
        /// </summary>
        public Thickness OuterMarginSizeThickness { get { return new Thickness(OuterMarginSize); } }

        /// <summary>
        /// The radius of the edges of the window
        /// </summary>
        public int WindowRadius
        {
            get
            {
                // If it is maximized or docked, no border
                return Borderless ? 0 : mWindowRadius;
            }
            set
            {
                mWindowRadius = value;
            }
        }

        /// <summary>
        /// The radius of the edges of the window
        /// </summary>
        public CornerRadius WindowCornerRadius { get { return new CornerRadius(WindowRadius); } }

        /// <summary>
        /// The height of the title bar / caption of the window
        /// </summary>
        public int TitleHeight { get; set; } = 42;
        /// <summary>
        /// The height of the title bar / caption of the window
        /// </summary>
        public GridLength TitleHeightGridLength { get { return new GridLength(TitleHeight + ResizeBorder); } }

        /// <summary>
        /// The current page of the application
        /// </summary>
        public ApplicationPage CurrentPage { get; set; } = ApplicationPage.Dashboard;

        public SideMenuContent CurrentSideMenuContent { get; set; } = SideMenuContent.Dashboard;

        #endregion

        #region Commands

        /// <summary>
        /// The command to minimize the window
        /// </summary>
        public ICommand MinimizeCommand { get; set; }

        /// <summary>
        /// The command to maximize the window
        /// </summary>
        public ICommand MaximizeCommand { get; set; }

        /// <summary>
        /// The command to close the window
        /// </summary>
        public ICommand CloseCommand { get; set; }

        /// <summary>
        /// The command to show the system menu of the window
        /// </summary>
        public ICommand MenuCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Dashboard
        /// </summary>
        public ICommand OpenDashboardCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Bot
        /// </summary>
        public ICommand OpenBotCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Profile
        /// </summary>
        public ICommand OpenProfileCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Contact
        /// </summary>
        public ICommand OpenContactCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Settings
        /// </summary>
        public ICommand OpenSettingsCommand { get; set; }

        /// <summary>
        /// The command to change the side menu to the Settings
        /// </summary>
        public ICommand OpenSubscribeCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public WindowViewModel(Window window)
        {
            mWindow = window;

            // Listen out for the window resizing
            mWindow.StateChanged += (sender, e) =>
            {
                // Fire off events for all properties that are affected by a resize
                WindowResized();
            };

            // Create commands
            MinimizeCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Minimized);
            MaximizeCommand = new RelayCommand(() => mWindow.WindowState ^= WindowState.Maximized);
            CloseCommand = new RelayCommand(() => mWindow.Close());
            MenuCommand = new RelayCommand(() => SystemCommands.ShowSystemMenu(mWindow, GetMousePosition()));
            OpenDashboardCommand = new RelayCommand(OpenDashboard);
            OpenBotCommand = new RelayCommand(OpenBot);
            OpenProfileCommand = new RelayCommand(OpenProfile);
            OpenContactCommand = new RelayCommand(OpenContact);
            OpenSubscribeCommand = new RelayCommand(OpenSubscribe);
            OpenSettingsCommand = new RelayCommand(OpenSettings);

            // Fix window resize issue
            mWindowResizer = new WindowResizer(mWindow);

            // Listen out for dock changes
            mWindowResizer.WindowDockChanged += (dock) =>
            {
                // Store last position
                mDockPosition = dock;

                // Fire off resize events
                WindowResized();
            };
        }

        #endregion

        #region Command Methods
        
        /// <summary>
        /// Changes the current side menu to the Dashboard
        /// </summary>
        public void OpenDashboard()
        {
            CurrentSideMenuContent = SideMenuContent.Dashboard;
            GoToPage(ApplicationPage.Dashboard);
        }

        /// <summary>
        /// Changes the current side menu to the Bot
        /// </summary>
        public void OpenBot()
        {
            CurrentSideMenuContent = SideMenuContent.Bot;
            GoToPage(ApplicationPage.Bot);
        }

        /// <summary>
        /// Changes the current side menu to the Profile
        /// </summary>
        public void OpenProfile()
        {
            CurrentSideMenuContent = SideMenuContent.Profile;
            GoToPage(ApplicationPage.Profile);
        }

        /// <summary>
        /// Changes the current side menu to the Contact
        /// </summary>
        public void OpenContact()
        {
            CurrentSideMenuContent = SideMenuContent.Contact;
            GoToPage(ApplicationPage.Contact);
        }

        /// <summary>
        /// Changes the current side menu to the Settings
        /// </summary>
        public void OpenSettings()
        {
            CurrentSideMenuContent = SideMenuContent.Settings;
            GoToPage(ApplicationPage.Settings);
        }

        /// <summary>
        /// Open browser to subscribe page
        /// </summary>
        public void OpenSubscribe()
        {
            Process.Start("explorer", "https://dashapi13.herokuapp.com/about");
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Gets the current mouse position on the screen
        /// </summary>
        /// <returns></returns>
        private Point GetMousePosition()
        {
            // Position of the mouse relative to the window
            var position = Mouse.GetPosition(mWindow);

            // Add the window position so its a "ToScreen"
            if (mWindow.WindowState == WindowState.Maximized)
                return new Point(position.X + mWindowResizer.CurrentMonitorSize.Left, position.Y + mWindowResizer.CurrentMonitorSize.Top);
            else
                return new Point(position.X + mWindow.Left, position.Y + mWindow.Top);
        }

        /// <summary>
        /// If the window resizes to a special position (docked or maximized)
        /// this will update all required property change events to set the borders and radius values
        /// </summary>
        private void WindowResized()
        {
            // Fire off events for all properties that are affected by a resize
            OnPropertyChanged(nameof(Borderless));
            OnPropertyChanged(nameof(ResizeBorderThickness));
            OnPropertyChanged(nameof(OuterMarginSize));
            OnPropertyChanged(nameof(OuterMarginSizeThickness));
            OnPropertyChanged(nameof(WindowRadius));
            OnPropertyChanged(nameof(WindowCornerRadius));
        }


        #endregion

        #region Helper Methods

        /// <summary>
        /// Navigates to the specified page
        /// </summary>
        /// <param name="page">The page to go to</param>
        public void GoToPage(ApplicationPage page)
        {
            //See if page has changed
            var different = CurrentPage != page;

            //Set the current page
            CurrentPage = page;

            // If the page hasn't changed, fire off notification
            // So pages still update if just the view model has changed
            if (!different)
                OnPropertyChanged(nameof(CurrentPage));
        }

        #endregion
    }
}
