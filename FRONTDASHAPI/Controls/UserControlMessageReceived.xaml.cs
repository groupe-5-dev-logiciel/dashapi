﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour UserControlMessageReceived.xaml
    /// </summary>
    public partial class UserControlMessageReceived : UserControl
    {
        public UserControlMessageReceived()
        {
            InitializeComponent();
            //setUser();
        }

        public async void setUser()
        {
            User user = new User();

            user = await user.GetUserAsync();

            if (user.id == App.id)
            {
                BotMessage.Text = "Bonjour Mr." + user.last_name + " ! ";
            }
            else
            {
                MessageBox.Show("Erreur - API");
            }
        }
    }
}
