﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour Profile.xaml
    /// </summary>
    public partial class Profile : Page
    {
        public Profile()
        {
            InitializeComponent();
            setUser();
        }

        public string ImageURL
        {
            get { return (string)GetValue(ImageURLProperty); }
            set { SetValue(ImageURLProperty, value); }
        }

        public static readonly DependencyProperty ImageURLProperty = DependencyProperty.Register("ImageURL", typeof(string), typeof(Profile), new PropertyMetadata(""));

        public async void setUser()
        {
            User user = new User();

            user = await user.GetUserAsync();

            if (user.id == App.id)
            {
                abonnements.Text = user.abonnement;
                lastname.Text = user.last_name;
                firstname.Text = user.first_name;
                email.Text = user.email;
                ImageURL = $"https://eu.ui-avatars.com/api/?name={user.last_name}+{user.first_name}?size=2048";
            }
            else
            {
                MessageBox.Show("Erreur - API");
            }
        }
    }
}
