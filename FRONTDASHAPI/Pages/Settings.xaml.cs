﻿using Newtonsoft.Json.Linq;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour Page2.xaml
    /// </summary>
    public partial class Settings : Page
    {
        public Settings()
        {
            InitializeComponent();
            SaveBtn.Click += (s, e) => SaveBtn_Click();
            spotifyBtn.Click += (s, e) => SpotifyBtn_Click();
            if (App.APIsSettings.spotifyAPISettings.spotifyClient == null)
            {
                spotifyBtn.Content = "Connexion";
                spotifyBtn.Background = Brushes.Green;
            }
            else
            {
                spotifyBtn.Content = "Connecté";
                spotifyBtn.Background = Brushes.Gray;
                spotifyBtn.IsEnabled = false;
            }
            ZipCode = App.APIsSettings.weatherAPISettings.ZipCode;
        }

        public async void PostSpotifyCodeAsync(string idUser, string code)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var jObject = (dynamic)new JObject();
                    jObject.idUser = idUser;
                    jObject.API_Type = "Spotify";
                    jObject.API_Token = code;
                    var content = new StringContent(jObject.ToString(), Encoding.UTF8, "application/json");
                    var res = await client.PostAsync("https://dashapi13.herokuapp.com/apiAccount/", content);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SpotifyBtn_Click()
        {
            ConnectToSpotify();
        }

        private static EmbedIOAuthServer _server;

        private async void ConnectToSpotify()
        {
            _server = new EmbedIOAuthServer(new Uri("http://localhost:5000/callback"), 5000);
            await _server.Start();

            _server.AuthorizationCodeReceived += OnAuthorizationCodeReceived;

            var request = new LoginRequest(_server.BaseUri, "4c7317ada5714030b34a045bbdbbde18", LoginRequest.ResponseType.Code)
            {
                Scope = new List<string> { Scopes.UserReadPlaybackState, Scopes.UserModifyPlaybackState, Scopes.UserReadCurrentlyPlaying, Scopes.UserLibraryRead, Scopes.UserTopRead }
            };
            BrowserUtil.Open(request.ToUri());
        }

        private async Task OnAuthorizationCodeReceived(object sender, AuthorizationCodeResponse response)
        {
            await _server.Stop();
            var config = SpotifyClientConfig.CreateDefault();
            var tokenResponse = await new OAuthClient(config).RequestToken(
              new AuthorizationCodeTokenRequest(
                "4c7317ada5714030b34a045bbdbbde18", "d721c96c445f4ef58d086591464d419f", response.Code, new Uri("http://localhost:5000/callback")
              )
            );

            PostSpotifyCodeAsync(App.id.ToString(), tokenResponse.RefreshToken);

            App.APIsSettings.spotifyAPISettings.spotifyClient = new SpotifyClient(tokenResponse.AccessToken);

            spotifyBtn.Content = "Connecté";
            spotifyBtn.Background = Brushes.Gray;
            spotifyBtn.IsEnabled = false;
        }

        private void SaveBtn_Click()
        {
            if (ZipCode.Length == 5)
            {
                App.APIsSettings.weatherAPISettings.SaveZipCode(ZipCode);
            }
            else
            {
                MessageBox.Show("Rentrez un code postal valide");
            }
        }

        public string ZipCode
        {
            get { return (string)GetValue(ZipCodeProperty); }
            set { SetValue(ZipCodeProperty, value); }
        }

        public static readonly DependencyProperty ZipCodeProperty = DependencyProperty.Register("ZipCode", typeof(string), typeof(Settings), new PropertyMetadata(""));
    }
}
