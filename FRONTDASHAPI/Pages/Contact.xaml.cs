﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour Contact.xaml
    /// </summary>
    public partial class Contact : Page
    {
        public Contact()
        {
            InitializeComponent();
            setUser();
        }

        public async void setUser()
        {
            User user = new User();

            user = await user.GetUserAsync();

            if (user.id == App.id)
            {
                lastname.Text = user.last_name;
                firstname.Text = user.first_name;
                email.Text = user.email;
            }
            else
            {
                MessageBox.Show("Erreur - API");
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            string to = "dashapi.admin-20129c@inbox.mailtrap.io";
            string from = email.Text;
            string subject = "Demande via le formulaire de contact";
            string body = content.Text;

            var client = new SmtpClient("smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("ea6da37da90f36", "93432f82c0c5e3"),
                EnableSsl = true
            };

            try
            {
                client.Send(from, to, subject, body);
                Console.WriteLine("Sent");
            } catch (Exception ex)
            {
                Console.WriteLine("Exception provoqué par : {0}", ex.ToString());
            }
        }
    }
}
