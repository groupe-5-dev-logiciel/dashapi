﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour Bot.xaml
    /// </summary>
    public partial class Bot : Page
    {
        /// <summary>
        /// Constructeur de la page du Bot
        /// </summary>
        public Bot()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Affichage de la commande dans la bulle de l'utilisateur
            string visibleUserCommand = userCommand.Text;

            if (visibleUserCommand.StartsWith("/"))
                visibleUserCommand = visibleUserCommand.Substring(1);

            userMessage.Visibility = Visibility.Visible;
            userMessage.UserMessage.Text = visibleUserCommand;
            userMessage.SendingTime.Text = DateTime.Now.ToString("dd/MM HH:mm");

            //Initialisation des variables
            BotController botController = new BotController();

            //Recherche de la réponse par le Bot
            string response = botController.sendResponse(userCommand.Text);

            //Affichage de la réponse
            botMessage.Visibility = Visibility.Visible;
            botMessage.BotMessage.Text = response;
            botMessage.ReceivedTime.Text = DateTime.Now.ToString("dd/MM HH:mm");

            //Reset de la TextBox
            userCommand.Text = "";
        }
    }
}
