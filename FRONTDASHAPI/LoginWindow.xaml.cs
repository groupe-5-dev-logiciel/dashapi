﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Logique d'interaction pour LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        static HttpClient client = new HttpClient();

        public class Logon
        {
            public string message { get; set; }
            public int id { get; set; }
            public bool success { get; set; }
        }

        static async Task<Logon> GetUserAsync(HttpRequestMessage request)
        {
            Logon logon = new Logon();
            HttpResponseMessage response = await client.PostAsync("https://dashapi13.herokuapp.com/users/login", request.Content);
            response.EnsureSuccessStatusCode();

            logon = await response.Content.ReadAsAsync<Logon>();

            return logon;
        }

        public LoginWindow()
        {
            InitializeComponent();
            email.Text = "gaetan.pat@free.fr";
            password.Password = "password";
        }

        public void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        public void Window_Reset(bool success)
        {
            if(success == true)
            {
                email.Text = "";
                password.Password = "";
                errorMessage.Text = "";
            } else
            {
                password.Password = "";
                errorMessage.Text = "";
            }
        }

        private void closeLoginWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private async void userLogin_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(email.Text) && String.IsNullOrEmpty(password.Password))
            {
                errorMessage.Text = "Renseignez le formulaire de connexion";
            }
            else if (String.IsNullOrEmpty(email.Text))
            {
                errorMessage.Text = "Renseignez votre Email";
            } else if (String.IsNullOrEmpty(password.Password))
            {
                errorMessage.Text = "Renseignez votre mot de passe";
            } else
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "/path/to/post/to");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("email", email.Text));
                keyValues.Add(new KeyValuePair<string, string>("password", password.Password));

                request.Content = new FormUrlEncodedContent(keyValues);

                //Logon logon = new Logon();
                Logon logon = await GetUserAsync(request);

                if (logon.success == true)
                {
                    this.Window_Reset(logon.success);

                    App.id = logon.id;
                    App.openMainWindow();

                    this.Close();
                }
                else
                {
                    this.Window_Reset(logon.success);
                    errorMessage.Text = logon.message;
                }
            }
        }

        private void view_register_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer","https://dashapi13.herokuapp.com/sign-up");
        }
    }
}
