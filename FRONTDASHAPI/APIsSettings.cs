﻿using FRONTDASHAPI.WpfDashboardControl.Widgets;
using Newtonsoft.Json.Linq;
using SpotifyAPI.Web;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FRONTDASHAPI
{
    /// <summary>
    /// Paramètres configurables des APIs
    /// </summary>
    public class APIsSettings
    {
        public WeatherAPISettings weatherAPISettings = new WeatherAPISettings();
        public SpotifyAPISettings spotifyAPISettings = new SpotifyAPISettings();

        /// <summary>
        /// Paramètres du widget de la Météo
        /// </summary>
        public class WeatherAPISettings
        {
            public async void SaveZipCode(string zipCode)
            {
                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        using (HttpResponseMessage res = await client.GetAsync(@$"https://api-adresse.data.gouv.fr/search/?q={zipCode}"))
                        {
                            using (HttpContent content = res.Content)
                            {
                                var data = await content.ReadAsStringAsync();
                                if (data != null)
                                {
                                    var parsedData = JObject.Parse(data);
                                    City = parsedData["features"][0]["properties"]["label"].ToString();
                                    Longitude = parsedData["features"][0]["geometry"]["coordinates"][0].ToString();
                                    Latitude = parsedData["features"][0]["geometry"]["coordinates"][1].ToString();
                                    ZipCode = zipCode;
                                    SettingsUtils.SaveSettings();
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }

            /// <summary>
            /// Nom de la ville
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// Coordonnée de latitude
            /// </summary>
            public string Latitude { get; set; }

            /// <summary>
            /// Coordonnée de longitude
            /// </summary>
            public string Longitude { get; set; }

            /// <summary>
            /// Zip code
            /// </summary>
            public string ZipCode { get; set; }
        }

        public class SpotifyAPISettings
        {
            public SpotifyClient spotifyClient = null;            
        }
    }

    public static class SettingsUtils
    {
        /// <summary>
        /// Chargement des valeurs des paramètres
        /// </summary>
        public static void LoadSettings()
        {
            XmlSerializer xs = new XmlSerializer(typeof(APIsSettings.WeatherAPISettings));
            if (File.Exists("apissettings.xml"))
            {
                using (StreamReader rd = new StreamReader("apissettings.xml"))
                {
                    if (!rd.EndOfStream)
                    {
                        App.APIsSettings.weatherAPISettings = xs.Deserialize(rd) as APIsSettings.WeatherAPISettings;
                    }
                }
                GetSpotifyCodeAsync(App.id.ToString());
            }
        }

        /// <summary>
        /// Enregistrement des valeurs des paramètres
        /// </summary>
        public static void SaveSettings()
        {
            XmlSerializer xs = new XmlSerializer(typeof(APIsSettings.WeatherAPISettings));
            using (StreamWriter wr = new StreamWriter("apissettings.xml"))
            {
                xs.Serialize(wr, App.APIsSettings.weatherAPISettings);
            }
        }

        public static async void GetSpotifyCodeAsync(string idUser)
        {
            string spotifyCode = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage res = await client.GetAsync($"https://dashapi13.herokuapp.com/apiAccount/"))
                    {
                        if (res.IsSuccessStatusCode)
                        {
                            var data = await res.Content.ReadAsStringAsync();
                            var parsedData = JObject.Parse(data);
                            foreach (var item in parsedData["result"])
                            {
                                if (((string)item["idUser"] == idUser) && ((string)item["API_Type"] == "Spotify"))
                                {
                                    spotifyCode = item["API_Token"].ToString();
                                    await GetNewToken(spotifyCode);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static async Task GetNewToken(string refreshToken)
        {
            var config = SpotifyClientConfig.CreateDefault();
            var tokenResponse = await new OAuthClient(config).RequestToken(
              new AuthorizationCodeRefreshRequest(
                "4c7317ada5714030b34a045bbdbbde18", "d721c96c445f4ef58d086591464d419f", refreshToken)
            );

            App.APIsSettings.spotifyAPISettings.spotifyClient = new SpotifyClient(tokenResponse.AccessToken);
        }
    }
}
