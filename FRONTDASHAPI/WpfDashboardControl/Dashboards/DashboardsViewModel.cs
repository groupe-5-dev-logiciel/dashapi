﻿using FRONTDASHAPI.WpfDashboardControl.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using FRONTDASHAPI.WpfDashboardControl.Resources;
using FRONTDASHAPI.WpfDashboardControl.Resources.DashboardControl;
using FRONTDASHAPI.WpfDashboardControl.Widgets;
using System;
using System.Xml.Serialization;
using System.IO;
using System.Collections.ObjectModel;

namespace FRONTDASHAPI.WpfDashboardControl.Dashboards
{
    /// <summary>
    /// View model for dashboards
    /// Implements the <see cref="Infrastructure.ViewModelBase" />
    /// </summary>
    /// <seealso cref="Infrastructure.ViewModelBase" />
    public class DashboardsViewModel : ViewModelBase
    {
        #region Private Fields

        private List<Widget> _availableWidgets = new List<Widget>();
        private TempWidgetHost _configuringWidget;
        private bool _editMode;
        private DashboardModel _dashboard = new DashboardModel();

        private void SaveWidgets()
        {
            Type[] types = new Type[] { typeof(Calendar_TwoByTwoViewModel), typeof(Weather_TwoByThreeViewModel), typeof(WeatherForecast_TwoByFourViewModel), typeof(SpotifyNewReleases_ThreeByTwoViewModel), typeof(SpotifyPlayer_OneByTwoViewModel), typeof(SpotifyTops_ThreeByTwoViewModel) };
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<WidgetBase>), types);
            using (StreamWriter wr = new StreamWriter("dashboard.xml"))
            {
                xs.Serialize(wr, Dashboard.Widgets);
            }
        }

        #endregion Private Fields

        #region Public Properties  

        /// <summary>
        /// Gets or sets the available widgets.
        /// </summary>
        /// <value>The available widgets.</value>
        public List<Widget> AvailableWidgets
        {
            get => _availableWidgets;
            set => RaiseAndSetIfChanged(ref _availableWidgets, value);
        }

        /// <summary>
        /// Gets the command add widget.
        /// </summary>
        /// <value>The command add widget.</value>
        public ICommand CommandAddWidget => new Infrastructure.RelayCommand(o =>
        {
            var widgetToAdd = (Widget)o;

            Dashboard.Widgets.Add(widgetToAdd.CreateWidget());
            SaveWidgets();
            EditMode = true;
        });

        /// <summary>
        /// Gets the command configure widget.
        /// </summary>
        /// <value>The command configure widget.</value>
        public ICommand CommandConfigureWidget => new Infrastructure.RelayCommand(o =>
        {
            var widgetHost = (WidgetHost)o;
            ConfiguringWidget = new TempWidgetHost
            {
                DataContext = widgetHost.DataContext,
                Content = widgetHost.DataContext,
                MaxWidth = widgetHost.MaxWidth,
                MaxHeight = widgetHost.MaxHeight,
                Width = widgetHost.ActualWidth,
                Height = widgetHost.ActualHeight
            };
        });

        /// <summary>
        /// Gets the command done configuring widget.
        /// </summary>
        /// <value>The command done configuring widget.</value>
        public ICommand CommandDoneConfiguringWidget => new Infrastructure.RelayCommand(o => ConfiguringWidget = null);

        /// <summary>
        /// Gets the command edit dashboard.
        /// </summary>
        /// <value>The command edit dashboard.</value>
        public ICommand CommandEditDashboard => new Infrastructure.RelayCommand(o => 
        {
            EditMode = o.ToString() == "True";
            SaveWidgets();
        }, o => ConfiguringWidget == null);

        /// <summary>
        /// Gets the command remove widget.
        /// </summary>
        /// <value>The command remove widget.</value>
        public ICommand CommandRemoveWidget => new Infrastructure.RelayCommand(o => {
            Dashboard.Widgets.Remove((WidgetBase)o);
            SaveWidgets();
        });

        /// <summary>
        /// Gets or sets the configuring widget.
        /// </summary>
        /// <value>The configuring widget.</value>
        public TempWidgetHost ConfiguringWidget
        {
            get => _configuringWidget;
            set => RaiseAndSetIfChanged(ref _configuringWidget, value);
        }

        /// <summary>
        /// Gets or sets a value indicating whether [edit mode].
        /// </summary>
        /// <value><c>true</c> if [edit mode]; otherwise, <c>false</c>.</value>
        public bool EditMode
        {
            get => _editMode;
            set => RaiseAndSetIfChanged(ref _editMode, value);
        }

        /// <summary>
        /// Gets or sets the dashboard.
        /// </summary>
        /// <value>The selected dashboard.</value>
        public DashboardModel Dashboard
        {
            get => _dashboard;
            set
            {
                if (!RaiseAndSetIfChanged(ref _dashboard, value))
                    return;
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <returns>Task.</returns>
        public Task Start()
        {
            Dashboard = _dashboard;

            AvailableWidgets = new List<Widget> {
                new Widget("Widget calendrier", "Affiche un calendrier.",
                    () => new Calendar_TwoByTwoViewModel()),
                new Widget("Widget metéo actuelle", "Fourni des informations sur la météo actuelle.",
                    () => new Weather_TwoByThreeViewModel()),
                new Widget("Widget prévision metéo", "Fourni des informations sur les prévisions météos.",
                    () => new WeatherForecast_TwoByFourViewModel()),
                new Widget("Widget Spotify: Player", "Controle la musique de votre Spotify.", 
                    () => new SpotifyPlayer_OneByTwoViewModel()),
                new Widget("Widget Spotify: Nouvelles sorties", "Fourni des informations sur les nouvelles sorties d'albums.",
                    () => new SpotifyNewReleases_ThreeByTwoViewModel()),
                new Widget("Widget Spotify: Top Artistes et Top Musiques", "Fourni des informations sur les artistes et les musiques que vous avez le plus écoutés",
                    () => new SpotifyTops_ThreeByTwoViewModel())
            };

            return Task.CompletedTask;
        }

        #endregion Public Methods
    }
}