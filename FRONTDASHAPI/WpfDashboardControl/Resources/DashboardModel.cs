﻿using FRONTDASHAPI.WpfDashboardControl.Infrastructure;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace FRONTDASHAPI.WpfDashboardControl.Resources
{
    /// <summary>
    /// Represents a dashboard model containing widgets and a title
    /// Implements the <see cref="Infrastructure.ViewModelBase" />
    /// </summary>
    /// <seealso cref="Infrastructure.ViewModelBase" />
    public class DashboardModel : ViewModelBase
    {
        #region Private Fields
        private ObservableCollection<WidgetBase> _widgets = new ObservableCollection<WidgetBase>();

        #endregion Private Fields

        #region Public Properties

        public DashboardModel()
        {
            try
            {
                Type[] types = new Type[] { typeof(Widgets.Calendar_TwoByTwoViewModel), typeof(Widgets.Weather_TwoByThreeViewModel), typeof(Widgets.WeatherForecast_TwoByFourViewModel), typeof(Widgets.SpotifyNewReleases_ThreeByTwoViewModel), typeof(Widgets.SpotifyPlayer_OneByTwoViewModel), typeof(Widgets.SpotifyTops_ThreeByTwoViewModel) };
                XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<WidgetBase>), types);
                if(File.Exists("dashboard.xml"))
                {
                    using (StreamReader rd = new StreamReader("dashboard.xml"))
                    {
                        if (!rd.EndOfStream)
                        {
                            Widgets = xs.Deserialize(rd) as ObservableCollection<WidgetBase>;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets or sets the widgets.
        /// </summary>
        /// <value>The widgets.</value>
        public ObservableCollection<WidgetBase> Widgets
        {
            get => _widgets;
            set => RaiseAndSetIfChanged(ref _widgets, value);
        }

        #endregion Public Properties
    }
}