﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Windows;
using Newtonsoft.Json.Linq;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    /// <summary>
    /// Logique d'interaction pour WeatherForecast_TwoByFourView.xaml
    /// </summary>
    public partial class WeatherForecast_TwoByFourView
    {
        public WeatherForecast_TwoByFourView()
        {
            InitializeComponent();

            City = App.APIsSettings.weatherAPISettings.City;
            GetWeatherDataAsync();
        }

        public List<DailyWeatherForecast> weatherForecasts = new List<DailyWeatherForecast>();

        public string City
        {
            get { return (string)GetValue(CityProperty); }
            set { SetValue(CityProperty, value); }
        }

        public static readonly DependencyProperty CityProperty = DependencyProperty.Register("City", typeof(string), typeof(WeatherForecast_TwoByFourView), new PropertyMetadata(""));

        public async void GetWeatherDataAsync()
        {
            var lat = App.APIsSettings.weatherAPISettings.Latitude ?? "43,3";
            var lon = App.APIsSettings.weatherAPISettings.Longitude ?? "5,4";

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage res = await client.GetAsync($"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude=current,minutely,hourly,alerts&units=metric&lang=fr&appid=6c75395d09f4a804233cf32eda690ae2"))
                    {
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();

                            if (data != null)
                            {
                                var parsedData = JObject.Parse(data);
                                //System.Diagnostics.Debug.WriteLine(parsedData);

                                foreach (var day in parsedData["daily"])
                                {
                                    var dailyWeatherForecast = new DailyWeatherForecast();
                                    var unixTime = (long)day["dt"];
                                    var dateTime = DateTimeOffset.FromUnixTimeSeconds(unixTime);
                                    var dateTimeInfo = new CultureInfo("fr-FR").DateTimeFormat;
                                    dailyWeatherForecast.DayOfWeek = $"{dateTimeInfo.GetAbbreviatedDayName(dateTime.DayOfWeek)}";
                                    dailyWeatherForecast.Date = $"{dateTime.Day}/{dateTime.Month}";
                                    dailyWeatherForecast.TemperatureMin = $"{day["temp"]["min"]} °C";
                                    dailyWeatherForecast.TemperatureMax = $"{day["temp"]["max"]} °C";
                                    var icon = day["weather"][0]["icon"].ToString();
                                    dailyWeatherForecast.WeatherIcon = @"http://openweathermap.org/img/wn/" + icon + "@2x.png";

                                    weatherForecasts.Add(dailyWeatherForecast);
                                }

                                System.Diagnostics.Debug.WriteLine(weatherForecasts.Count);

                                icForecastList.ItemsSource = weatherForecasts;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public class DailyWeatherForecast
    {
        public string DayOfWeek { get; set; }
        public string Date { get; set; }
        public string TemperatureMin { get; set; }
        public string TemperatureMax { get; set; }
        public string WeatherIcon { get; set; }
    }
}
