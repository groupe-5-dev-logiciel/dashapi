﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.IconPacks;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    /// <summary>
    /// Logique d'interaction pour SpotifyPlayer_OneByTwoView.xaml
    /// </summary>
    public partial class SpotifyPlayer_OneByTwoView : UserControl
    {
        private SpotifyClient spotifyClient { get; set; }
        public SpotifyPlayer_OneByTwoView()
        {
            InitializeComponent();
            playBtn.Click += PlayBtn_Click;
            prevBtn.Click += (s, e) =>
            {
                PrevTrack();
                UpdatePlayer();
            };
            nextBtn.Click += (s, e) =>
            {
                NextTrack();
                UpdatePlayer();
            };
            var threadInit = new Thread(new ThreadStart(InitPlayer));
            threadInit.Start();
        }

        public string TrackName
        {
            get { return (string)GetValue(TrackNameProperty); }
            set { SetValue(TrackNameProperty, value); }
        }

        public static readonly DependencyProperty TrackNameProperty = DependencyProperty.Register("TrackName", typeof(string), typeof(SpotifyPlayer_OneByTwoView), new PropertyMetadata(""));

        public string ArtistName
        {
            get { return (string)GetValue(ArtistNameProperty); }
            set { SetValue(ArtistNameProperty, value); }
        }

        public static readonly DependencyProperty ArtistNameProperty = DependencyProperty.Register("ArtistName", typeof(string), typeof(SpotifyPlayer_OneByTwoView), new PropertyMetadata(""));

        public string ImageURL
        {
            get { return (string)GetValue(ImageURLProperty); }
            set { SetValue(ImageURLProperty, value); }
        }

        public static readonly DependencyProperty ImageURLProperty = DependencyProperty.Register("ImageURL", typeof(string), typeof(SpotifyPlayer_OneByTwoView), new PropertyMetadata(""));

        private void PlayBtn_Click(object sender, RoutedEventArgs e)
        {

            switch (playIcon.Kind)
            {
                case PackIconBootstrapIconsKind.Play:
                    playIcon.Kind = PackIconBootstrapIconsKind.Pause;
                    ResumeTrack();
                    break;
                case PackIconBootstrapIconsKind.Pause:
                    playIcon.Kind = PackIconBootstrapIconsKind.Play;
                    PauseTrack();
                    break;
            }
        }

        private async void InitPlayer()
        {
            while (spotifyClient == null)
            {
                spotifyClient = App.APIsSettings.spotifyAPISettings.spotifyClient;
            }

            var currentPlayback = await spotifyClient.Player.GetCurrentPlayback();
            if (currentPlayback != null)
            {
                if (currentPlayback.IsPlaying)
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        playIcon.Kind = PackIconBootstrapIconsKind.Pause;
                        var track = (FullTrack)currentPlayback.Item;
                        TrackName = track.Name;
                        ArtistName = track.Artists[0].Name;
                        ImageURL = track.Album.Images[0].Url;
                    }));
                }
                else
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        playIcon.Kind = PackIconBootstrapIconsKind.Play;
                    }));
                }
            }
        }

        private async void UpdatePlayer()
        {
            string trackName = TrackName;
            while (trackName == TrackName)
            {
                var currentPlayback = await spotifyClient.Player.GetCurrentPlayback();
                var track = (FullTrack)currentPlayback.Item;
                if (track.Name != TrackName)
                {
                    TrackName = track.Name;
                    ArtistName = track.Artists[0].Name;
                    ImageURL = track.Album.Images[0].Url;
                }
            }
        }

        public async void NextTrack()
        {
            await spotifyClient.Player.SkipNext();
        }

        public async void PrevTrack()
        {
            await spotifyClient.Player.SkipPrevious();
        }

        public async void PauseTrack()
        {
            await spotifyClient.Player.PausePlayback();
        }

        public async void ResumeTrack()
        {
            try
            {
                await spotifyClient.Player.ResumePlayback();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
