﻿using SpotifyAPI.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    /// <summary>
    /// Logique d'interaction pour SpotifyNewReleases_ThreebyTwoView.xaml
    /// </summary>
    public partial class SpotifyNewReleases_ThreebyTwoView : UserControl
    {
        public SpotifyNewReleases_ThreebyTwoView()
        {
            InitializeComponent();
            ConnectToApi();
        }

        public List<NewRelease> NewReleases = new List<NewRelease>();

        private async void ConnectToApi()
        {
            var config = SpotifyClientConfig.CreateDefault();

            var request = new ClientCredentialsRequest("4c7317ada5714030b34a045bbdbbde18", "d721c96c445f4ef58d086591464d419f");
            var response = await new OAuthClient(config).RequestToken(request);

            var spotify = new SpotifyClient(config.WithToken(response.AccessToken));

            var releases = await spotify.Browse.GetNewReleases();

            releases.Albums.Items.ForEach((album) => {
                var release = new NewRelease();
                release.ArtistName = album.Artists[0].Name;
                release.AlbumName = album.Name;
                release.ImageUrl = album.Images[0].Url;
                NewReleases.Add(release);
            });

            icNewReleases.ItemsSource = NewReleases;
        }
    }

    public class NewRelease
    {
        public string ImageUrl { get; set; }
        public string AlbumName { get; set; }
        public string ArtistName { get; set; }
    }
}
