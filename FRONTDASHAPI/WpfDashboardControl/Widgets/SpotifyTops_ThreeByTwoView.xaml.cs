﻿using SpotifyAPI.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    /// <summary>
    /// Logique d'interaction pour SpotifyTops_ThreeByTwoView.xaml
    /// </summary>
    public partial class SpotifyTops_ThreeByTwoView : UserControl
    {
        public SpotifyTops_ThreeByTwoView()
        {
            InitializeComponent();
            var threadInit = new Thread(new ThreadStart(ConnectToApi));
            threadInit.Start();
            topArtistsBtn.Click += (s,e) => TopArtistsBtn_Click();
            topTracksBtn.Click += (s,e) => TopTracksBtn_Click();
        }

        private void TopArtistsBtn_Click()
        {
            icTopArtists.Visibility = Visibility.Visible;
            icTopsTracks.Visibility = Visibility.Hidden;
        }

        private void TopTracksBtn_Click()
        {
            icTopsTracks.Visibility = Visibility.Visible;
            icTopArtists.Visibility = Visibility.Hidden;
        }

        public List<Track> TopTracks = new List<Track>();
        public List<Artist> TopArtists = new List<Artist>();

        /// <summary>
        /// Connexion à l'api, récupération des données des listes des tops artistes et top sons
        /// </summary>
        private async void ConnectToApi()
        {
            SpotifyClient spotifyClient = null;
            //récupere le client
            while (spotifyClient == null)
            {
                spotifyClient = App.APIsSettings.spotifyAPISettings.spotifyClient;
            }

            //Récupère les tops tracks
            var topTracks = await spotifyClient.Personalization.GetTopTracks();
            topTracks.Items.ForEach((track) =>
            {
                var newTrack = new Track();
                newTrack.ArtistName = track.Artists[0].Name;
                newTrack.TrackName = track.Name;
                newTrack.ImageUrl = track.Album.Images[0].Url;
                TopTracks.Add(newTrack);
            });

            //Récupère les tops artistes
            var topArtists = await spotifyClient.Personalization.GetTopArtists();
            topArtists.Items.ForEach((artist) =>
            {
                var newArtist = new Artist();
                newArtist.ArtistName = artist.Name;
                newArtist.Popularity = "popularité : " + artist.Popularity + "/100";
                newArtist.ImageUrl = artist.Images[0].Url;
                TopArtists.Add(newArtist);
            });

            Dispatcher.Invoke(new Action(() =>
            {
                //Charge les listes
                icTopsTracks.ItemsSource = TopTracks;
                icTopArtists.ItemsSource = TopArtists;
            }));
        }
    }

    public class Track
    {
        public string ImageUrl { get; set; }
        public string TrackName { get; set; }
        public string ArtistName { get; set; }
    }

    public class Artist
    {
        public string ImageUrl { get; set; }
        public string Popularity { get; set; }
        public string ArtistName { get; set; }
    }
}
