﻿using FRONTDASHAPI.WpfDashboardControl.Infrastructure;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    public class SpotifyPlayer_OneByTwoViewModel : WidgetBase
    {
        #region Private Fields

        private string _widgetTitle;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the widget title.
        /// </summary>
        /// <value>The widget title.</value>
        public sealed override string WidgetTitle
        {
            get => _widgetTitle;
            set => RaiseAndSetIfChanged(ref _widgetTitle, value);
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SpotifyPlayer_OneByTwoViewModel"/> class.
        /// </summary>
        public SpotifyPlayer_OneByTwoViewModel() : base(WidgetSize.OneByTwo)
        {
        }

        #endregion Public Constructors
    }
}

