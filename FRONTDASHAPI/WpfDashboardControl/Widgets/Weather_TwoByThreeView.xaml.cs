﻿using System;
using System.Linq;
using System.Net.Http;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json.Linq;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    /// <summary>
    /// Logique d'interaction pour Weather_TwoByThreeView.xaml
    /// </summary>
    public partial class Weather_TwoByThreeView
    {
        public Weather_TwoByThreeView()
        {
            InitializeComponent();
            GetWeatherDataAsync();
        }

        public string Temperature
        {
            get { return (string)GetValue(TemperatureProperty); }
            set { SetValue(TemperatureProperty, value); }
        }

        public static readonly DependencyProperty TemperatureProperty = DependencyProperty.Register("Temperature", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string TemperatureMin
        {
            get { return (string)GetValue(TemperatureMinProperty); }
            set { SetValue(TemperatureMinProperty, value); }
        }

        public static readonly DependencyProperty TemperatureMinProperty = DependencyProperty.Register("TemperatureMin", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string TemperatureMax
        {
            get { return (string)GetValue(TemperatureMaxProperty); }
            set { SetValue(TemperatureMaxProperty, value); }
        }

        public static readonly DependencyProperty TemperatureMaxProperty = DependencyProperty.Register("TemperatureMax", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string WindSpeed
        {
            get { return (string)GetValue(WindSpeedProperty); }
            set { SetValue(WindSpeedProperty, value); }
        }

        public static readonly DependencyProperty WindSpeedProperty = DependencyProperty.Register("WindSpeed", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string WindDirection
        {
            get { return (string)GetValue(WindDirectionProperty); }
            set { SetValue(WindDirectionProperty, value); }
        }

        public static readonly DependencyProperty WindDirectionProperty = DependencyProperty.Register("WindDirection", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string Humiditity
        {
            get { return (string)GetValue(HumiditityProperty); }
            set { SetValue(HumiditityProperty, value); }
        }

        public static readonly DependencyProperty HumiditityProperty = DependencyProperty.Register("Humiditity", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string Pressure
        {
            get { return (string)GetValue(PressureProperty); }
            set { SetValue(PressureProperty, value); }
        }

        public static readonly DependencyProperty PressureProperty = DependencyProperty.Register("Pressure", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string City
        {
            get { return (string)GetValue(CityProperty); }
            set { SetValue(CityProperty, value); }
        }

        public static readonly DependencyProperty CityProperty = DependencyProperty.Register("City", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));

        public string WeatherDescription
        {
            get { return (string)GetValue(WeatherDescriptionProperty); }
            set { SetValue(WeatherDescriptionProperty, value); }
        }

        public static readonly DependencyProperty WeatherDescriptionProperty = DependencyProperty.Register("WeatherDescription", typeof(string), typeof(Weather_TwoByThreeView), new PropertyMetadata(""));
        
        public async void GetWeatherDataAsync()
        {
            var city = App.APIsSettings.weatherAPISettings.City ?? "Marseille";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage res = await client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={city}&units=metric&lang=fr&appid=6c75395d09f4a804233cf32eda690ae2"))
                    {
                        using (HttpContent content = res.Content)
                        {
                            var data = await content.ReadAsStringAsync();

                            if (data != null)
                            {
                                var parsedData = JObject.Parse(data);
                                System.Diagnostics.Debug.WriteLine(parsedData);
                                Temperature = $"{parsedData["main"]["temp"]} °C";
                                TemperatureMin = $"{parsedData["main"]["temp_min"]} °C";
                                TemperatureMax = $"{parsedData["main"]["temp_max"]} °C";
                                Humiditity = $"{parsedData["main"]["humidity"]} %";
                                Pressure = $"{parsedData["main"]["pressure"]} hPa";
                                WindSpeed = $"{parsedData["wind"]["speed"]} km/h";
                                WindDirection = $"{parsedData["wind"]["deg"]} degrés";
                                City = city;
                                var icon = parsedData["weather"][0]["icon"].ToString();
                                weatherIconImage.Source = new BitmapImage(new Uri((@"http://openweathermap.org/img/wn/" + icon + "@2x.png"), UriKind.Absolute));
                                var weatherDescription = parsedData["weather"][0]["description"].ToString();
                                WeatherDescription = weatherDescription.First().ToString().ToUpper() + weatherDescription.Substring(1);
                            }
                        }
                    }
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
