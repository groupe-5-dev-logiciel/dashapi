﻿using FRONTDASHAPI.WpfDashboardControl.Infrastructure;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    public class Calendar_TwoByTwoViewModel : WidgetBase
    {
        #region Private Fields

        private string _widgetTitle;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the widget title.
        /// </summary>
        /// <value>The widget title.</value>
        public sealed override string WidgetTitle
        {
            get => _widgetTitle;
            set => RaiseAndSetIfChanged(ref _widgetTitle, value);
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Calendar_TwoByTwoViewModel"/> class.
        /// </summary>
        public Calendar_TwoByTwoViewModel() : base(WidgetSize.TwoByTwo)
        {
        }

        #endregion Public Constructors
    }
}
