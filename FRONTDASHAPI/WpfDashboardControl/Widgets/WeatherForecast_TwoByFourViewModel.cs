﻿using FRONTDASHAPI.WpfDashboardControl.Infrastructure;

namespace FRONTDASHAPI.WpfDashboardControl.Widgets
{
    public class WeatherForecast_TwoByFourViewModel : WidgetBase
    {
        #region Private Fields

        private string _widgetTitle;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the widget title.
        /// </summary>
        /// <value>The widget title.</value>
        public sealed override string WidgetTitle
        {
            get => _widgetTitle;
            set => RaiseAndSetIfChanged(ref _widgetTitle, value);
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WeatherForecast_TwoByFourViewModel"/> class.
        /// </summary>
        public WeatherForecast_TwoByFourViewModel() : base(WidgetSize.TwoByFour)
        {
        }

        #endregion Public Constructors
    }
}
